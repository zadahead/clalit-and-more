import { useState } from "react/cjs/react.development";
import LifeCycleFunc from "./LifeCycleFunc";

const Toggler = () => {
    const [isDisplay, setIsDisplay] = useState(true);

    const renderView = () => {
        if (isDisplay) {
            return <LifeCycleFunc />
        }
        return null;
    }
    return (
        <div>
            <h2>Toggler</h2>
            <button onClick={() => setIsDisplay(!isDisplay)}>Toggle</button>
            {renderView()}
        </div>
    )
}

export default Toggler;