import Modal from "Modal/Modal";
import { Box, Btn, Rows } from "UIKit";

import './Popup.css';

const Popup = (props) => {
    return (
        <Modal>
            <div className="Popup">
                <Box>
                    <Rows>
                        <div className="content">
                            {props.children}
                        </div>
                        <Btn onClick={props.onClose}>Close</Btn>
                    </Rows>
                </Box>
            </div>
        </Modal>
    )
}

export default Popup;