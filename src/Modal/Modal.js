import ReactDom from 'react-dom';

const Modal = (props) => {
    return ReactDom.createPortal(
        props.children,
        document.getElementById('modal')
    )
}

export default Modal;