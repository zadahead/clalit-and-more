import { useState } from "react";

const User = ({ id, name, phone }) => {
    const [isDisplay, setIsDisplay] = useState(true)

    const renderPhone = () => {
        if (isDisplay) {
            return <h3>{phone}</h3>
        }
        return null;
    }

    const handleTogglePhone = () => {
        setIsDisplay(!isDisplay);
    }
    return (
        <div>
            <h2>#{id}, {name}</h2>
            {renderPhone()}
            <button onClick={handleTogglePhone} >
                {isDisplay ? 'Hide' : 'Show'} Phone
            </button>
        </div>
    )
}

export default User;