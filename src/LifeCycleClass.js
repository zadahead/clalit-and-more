import React from "react";


//mounted
//unmounted
//update
//render

class LifeCycleClass extends React.Component {
    state = {
        count: 0
    }
    componentDidMount = () => {
        console.log('componentDidMount');
    }

    componentDidUpdate = () => {
        console.log('componentDidUpdate');
    }

    componentWillUnmount = () => {
        console.log('componentWillUnmount');
    }

    handleCount = () => {
        this.setState({
            count: this.state.count + 1
        })
    }

    render = () => {
        console.log('render');
        return (
            <div>
                <h1>LifeCycleClass, {this.state.count}</h1>
                <button onClick={this.handleCount}>Add</button>
            </div>
        )
    }
}

export default LifeCycleClass;