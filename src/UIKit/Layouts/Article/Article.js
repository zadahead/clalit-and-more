import { Rows } from "../Line/Line";

const Article = (props) => {
    return (
        <div className="Article">
            <Rows>
                <h1>{props.headline}</h1>
                <h2>{props.info}</h2>
                {props.children}
            </Rows>
        </div>
    )
}

export default Article;