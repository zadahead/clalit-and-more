import './Line.css';

const Line = (props) => {
    return (
        <div className={`Line ${props.className || ''}`}>
            {props.children}
        </div>
    )
}

export const Rows = (props) => {
    return <Line {...props} className="rows" />
}

export default Line;