const Icon = (props) => {
    return (
        <i className={`fas fa-${props.i}`} onClick={props.onClick}></i>
    )
}

export default Icon;