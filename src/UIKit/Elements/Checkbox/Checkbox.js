import { Icon } from 'UIKit';
import './Checkbox.css';

const Checkbox = (props) => {
    return (
        <div className='Checkbox' onClick={() => props.onChange(!props.checked)}>
            <Icon i={`${props.checked ? 'check-' : ''}square`} />
        </div>
    )
}

export default Checkbox;