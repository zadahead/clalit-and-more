import { useEffect, useRef, useState } from "react";
import { Line } from "UIKit";
import Icon from "../Icon";

const Dropdown = ({ list, selected, onChange }) => {
    const [isOpen, setIsOpen] = useState(false);
    const wrapRef = useRef();

    useEffect(() => {
        document.addEventListener('click', handlePageClick)

        return () => {
            document.removeEventListener('click', handlePageClick)
        }
    }, [])

    const handleToggle = () => {
        console.log('handleToggle');
        setIsOpen(!isOpen);
    }

    const handleSelect = (item) => {
        onChange(item.id);
        setIsOpen(false);
    }

    const handlePageClick = (e) => {
        console.log('handlePageClick');
        if (!wrapRef.current) { return }
        if (!wrapRef.current.contains(e.target)) {
            setIsOpen(false);
        }
    }

    const renderList = () => {
        return list.map(i => {
            return (
                <div key={i.id} onClick={() => handleSelect(i)}>{i.value}</div>
            )
        })
    }

    const renderSelected = () => {
        if (selected) {
            const item = list.find(i => i.id === selected);
            if (item) {
                return item.value;
            }
        }
        return 'Please Select';
    }

    return (
        <div className="Dropdown" ref={wrapRef}>
            <div className="title" onClick={handleToggle}>
                <Line className="between">
                    <h2>{renderSelected()}</h2>
                    <Icon i="chevron-down" />
                </Line>
            </div>
            {isOpen &&
                <div className="list">
                    {renderList()}
                </div>
            }
        </div>
    )
}

export default Dropdown;