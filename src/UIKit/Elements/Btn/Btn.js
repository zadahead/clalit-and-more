
import { Line, Icon } from 'UIKit';

import './Btn.css';

const Btn = (props) => {
    return (
        <button
            className='Btn'
            onClick={props.onClick}
        >
            <Line className="between">
                {props.children}
                {props.i && <Icon i={props.i} />}
            </Line>
        </button>
    )
}


export default Btn;