import './Input.css';

const Input = (props) => {
    return (
        <div className="Input">
            <input {...props} />
        </div>
    )
}

export default Input;