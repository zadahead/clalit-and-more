import React, { useState } from "react";

const Counter = () => {
    const [count, setCount] = useState(0) //< --App

    return (
        <div>
            <h2>Count, {count}</h2>
            <button onClick={() => setCount(count + 1)}>Add</button>
        </div>
    )
}


class CounterClass extends React.Component {
    state = {
        count: 0
    }

    handleCount = () => {
        this.setState({
            count: this.state.count + 1
        })
    }

    render = () => {
        return (
            <div>
                <h1>Count, {this.state.count}</h1>
                <button onClick={this.handleCount}>Add Count</button>
            </div>
        )
    }
}

export default Counter;