import { useState } from "react";
import { Box, Dropdown } from "UIKit";

const list = [
    { id: 1, value: 'mosh' },
    { id: 2, value: 'david' },
    { id: 3, value: 'dave' },
    { id: 4, value: 'ohad' }
]

const Users = () => {
    const [selected, setSelected] = useState(null);

    const handleSelect = (selectedId) => {
        setSelected(selectedId);
    }

    return (
        <div>
            <h1>Dropdown</h1>
            <Box info="dropdown">
                <Dropdown list={list} selected={selected} onChange={handleSelect} />
            </Box>
        </div>
    )
}

export default Users;