import { useState } from "react";
import { Box, Article, Rows, Btn, Checkbox, Input } from "UIKit";

const Main = () => {
    const [checked, setChecked] = useState(true);
    const [value, setValue] = useState('');

    const handleClick = () => {
        console.log('clicked')
    }

    return (
        <Box>
            <Article headline="Great Article" info="some info">
                <Rows>
                    <Btn onClick={handleClick}>Click Me</Btn>
                    <Checkbox checked={checked} onChange={setChecked} />
                    <Input value={value} onChange={setValue} />
                </Rows>
            </Article>
        </Box>
    )
}

export default Main;