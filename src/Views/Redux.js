import CounterDisplay from 'Components/CounterDisplay';
import { Line } from 'UIKit';

const Redux = () => {
    return (
        <div>
            <h2>Redux</h2>
            <Line>
                <CounterDisplay />
            </Line>
        </div>
    )
}

export default Redux;