import { useNavigate } from "react-router-dom";

const Login = () => {
    let navigate = useNavigate();

    const handleLogin = () => {
        navigate('users');
    }

    return (
        <div>
            <h1>Login</h1>
            <button onClick={handleLogin}>Login</button>
        </div>
    )
}

export default Login;