import Modal from "Modal/Modal";
import Popup from "Modal/Popup/Popup";
import { useRef, useState } from "react";
import { Box, Article, Rows } from "UIKit";

const About = () => {
    const [value, setValue] = useState('');
    const [isPop, setIsPop] = useState(false);

    console.log('RENDER');

    const inputRef = useRef();
    const countRef = useRef(0);

    const handleFocus = () => {
        inputRef.current.select();
        countRef.current = countRef.current + 1;
        console.log(countRef.current);
    }

    const handleShowPopup = () => {
        setIsPop(true);
    }

    const handleHidePopup = () => {
        setIsPop(false);
    }

    return (
        <Box>
            <Article headline="Great Article" info="some info">
                <Rows>
                    <h1>About</h1>
                    <input ref={inputRef} value={value} onChange={(e) => setValue(e.target.value)} />
                    <button onClick={handleFocus}>Focus</button>
                    <button onClick={handleShowPopup}>Popup</button>
                </Rows>
                {isPop &&
                    <Popup onClose={handleHidePopup}>
                        <h1>Hello from Portal</h1>
                    </Popup>
                }
            </Article>
        </Box>
    )
}

export default About;