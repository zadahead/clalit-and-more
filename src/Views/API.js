import axios from "axios";
import { useEffect, useState } from "react";

const useFetch = (url) => {
    const [list, setList] = useState(null);

    useEffect(() => {
        //compnent mounted (user see's "loading...")
        fetchData();
    }, [])

    const fetchData = async () => {
        const resp = await axios.get(url)
        console.log(resp.data);
        setList(resp.data);
    }

    const fetchData2 = () => {
        axios.get(url).then(resp => {
            console.log(resp.data);
        }).catch(err => {

        })
    }

    return list;
}
const API = () => {
    const list = useFetch('https://jsonplaceholder.typicode.com/todos');

    const renderList = () => {
        if (!list) {
            return <h3>loading...</h3>
        }
        return list.map(i => {
            return <h3 key={i.id}>{i.title}</h3>
        })
    }
    return (
        <div>
            <h2>API</h2>
            {renderList()}
        </div>
    )
}

export default API;