import { useParams } from "react-router-dom";

const User = () => {
    const params = useParams();

    console.log(params);

    return (
        <div>
            <h1>User, {params.id}</h1>
        </div>
    )
}

export default User;