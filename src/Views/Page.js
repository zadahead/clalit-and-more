import CounterAdd from "Components/CounterAdd";
import Counter from "Counter";
import { NavLink, Route, Routes } from "react-router-dom";
import { Grid, Icon, Line } from "UIKit";
import API from "./API";
import Hooks, { Windowidth } from "./Hooks";
import Redux from "./Redux";
import Todos from "./Todos";
import User from "./User";
import Users from "./Users";

const Page = () => {

    return (
        <div className="App">
            <Grid>
                <div>
                    <Line className="between">
                        <Line>
                            <div>logo</div>
                            <Icon i="heart" />
                            <CounterAdd />
                        </Line>
                        <Line>
                            <NavLink to="/hooks/window">Window</NavLink>
                            <NavLink to="/dropdown">Dropdown</NavLink>
                            <NavLink to="/hooks">Hooks</NavLink>
                            <NavLink to="/api">API</NavLink>
                            <NavLink to="/redux">Redux</NavLink>
                            <NavLink to="/todos">Todos</NavLink>
                        </Line>
                    </Line>
                </div>
                <div>
                    <Routes>
                        <Route path="/layouts/*" element={<h1>Layouts</h1>} />
                        <Route path="/layouts/:id" element={<h1>Layouts With ID</h1>} />
                        <Route path="/layouts/mosh" element={<h1>Layouts With Mosh</h1>} />

                        <Route path="/dropdown" element={<Users />} />
                        <Route path="/users/:bksksksk/:project" element={<User />} />

                        <Route path="/hooks" element={<Hooks />} />
                        <Route path="/hooks/window" element={<Windowidth />} />
                        <Route path="/api" element={<API />} />
                        <Route path="/redux" element={<Redux />} />
                        <Route path="/todos" element={<Todos />} />
                    </Routes>
                </div>
            </Grid>
        </div>
    )
}

export default Page;