import { useInput } from "Hooks/useInput";
import { useWindowWidth } from "Hooks/useWindowWidth";

const Hooks = () => {
    const username = useInput('');
    const password = useInput('');


    return (
        <div>
            <h1>Custom Hooks</h1>
            <input {...username} placeholder="username" />
            <input
                value={password.value}
                onChange={password.onChange}
                placeholder="password"
                type="password"
            />
        </div>
    )
}


export const Windowidth = () => {
    //logic
    const width = useWindowWidth();

    //render
    return (
        <div>
            <h1>Window Width</h1>
            <h2>{width}</h2>
        </div>
    )
}

export default Hooks;