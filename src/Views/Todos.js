import { useInput } from "Hooks/useInput";
import { useDispatch, useSelector } from "react-redux";
import { Box, Btn, Checkbox, Icon, Input, Line } from "UIKit";

import { deleteTodos, fetchTodos, patchTodos, postTodos } from "State/todos";
import { useEffect, useRef } from "react";

const Todos = () => {
    const input = useInput('');
    const todos = useSelector(state => state.todos);
    const dispatch = useDispatch();

    const ulWrap = useRef();

    useEffect(() => {
        dispatch(fetchTodos());
    }, [])

    useEffect(() => {
        if (input.value) {
            ulWrap.current.scrollTop = ulWrap.current.scrollHeight;
            input.setValue('');
        }
    }, [todos])

    const handleToggle = (i) => {
        dispatch(patchTodos(i.id));
    }

    const handleDelete = (i) => {
        dispatch(deleteTodos(i.id));
    }

    const handleAdd = () => {
        if (!input.value) { return }
        dispatch(postTodos(input.value));
    }

    const getClass = (i) => {
        return i.completed ? 'shade' : '';
    }

    if (!todos || !todos.length) { return <h1>loading...</h1> }

    return (
        <div>
            <Box>
                <h1>Todos:</h1>
                <ul ref={ulWrap}>
                    {todos.map(i => (
                        <li key={i.id}>
                            <Line className="between">
                                <Line>
                                    <Checkbox checked={i.completed} onChange={() => handleToggle(i)} />
                                    <h4 className={getClass(i)}>{i.title}</h4>
                                </Line>
                                <Icon i="trash" onClick={() => handleDelete(i)} />
                            </Line>
                        </li>
                    ))}
                </ul>
                <Line>
                    <Input {...input} placeholder="Add New..." />
                    <Btn onClick={handleAdd}>Add</Btn>
                </Line>
            </Box>
        </div>
    )
}

export default Todos;