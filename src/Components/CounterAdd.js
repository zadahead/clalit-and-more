import { useDispatch } from "react-redux";
import { addCount } from "State/counter";
import { Line } from "UIKit";

import { fetchTodos } from "State/todos";

const CounterAdd = () => {
    const dispatch = useDispatch();

    const handleAdd = () => {
        dispatch(addCount());
    }

    const handleFetchTodos = () => {
        dispatch(fetchTodos());
    }

    return (
        <Line>
            <button onClick={handleAdd}>Add</button>
            <button onClick={handleFetchTodos}>Fetch Todos</button>
        </Line>
    )
}
export default CounterAdd;