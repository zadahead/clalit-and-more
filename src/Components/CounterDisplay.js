import { useSelector } from "react-redux";


const CounterDisplay = () => {
    const state = useSelector(state => state);
    console.log(state);

    const renderList = () => {
        if (state.todos && state.todos.length) {
            return state.todos.map(i => {
                return <div key={i.id}>{i.title}</div>
            })
        }
    }
    return (
        <div>
            <h2>Count, {state.count}</h2>
            {renderList()}
        </div>
    )
}
export default CounterDisplay;