import { useEffect, useState } from "react";

export const useWindowWidth = () => {
    const [width, setWidth] = useState(0);

    useEffect(() => {
        handleSetWidth();
        window.addEventListener('resize', handleSetWidth);

        return () => {
            window.removeEventListener('resize', handleSetWidth);
        }
    }, [])

    const handleSetWidth = () => {
        setWidth(window.innerWidth)
    }

    return width;
}