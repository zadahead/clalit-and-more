

//<User details={details} />
//{ id: 1, name: "mosh", phone: '1234' }
// #1, mosh - phone

import User from "./User";

//advanced
// #1, mosh <show phone>
// #1, mosh 1234 <hide phone>



const Users = () => {

    const list = [
        { id: 1, name: "mosh", phone: '1234' },
        { id: 2, name: "david", phone: '555' },
        { id: 3, name: "dave", phone: '6578' }
    ]

    const renderView = () => {
        return list.map(i => {
            return (
                <div key={i.id}>
                    <User {...i} />
                </div>
            );
        })
    }
    return (
        <div>
            <h2>Users List:</h2>
            <div>
                {renderView()}
            </div>
        </div>
    )
}


export default Users;