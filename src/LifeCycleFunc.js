//mounted
//unmounted
//update
//render

import { useEffect, useState } from "react";

const LifeCycleFunc = () => {
    const [count, setCount] = useState(0);
    const [color, setColor] = useState('red');

    //componentDidMount
    useEffect(() => {
        console.log('componentDidMount'); //1 mount

        return () => {
            console.log('componentWillUnmount'); //1 un-mount
        }
    }, [])

    //componentDidUpdate
    useEffect(() => {
        console.log('componentDidUpdate / componentDidMount', count); //2 mount, update

        return () => {
            console.log('componentWillUnmount - UPDATE', count); //1 last render un-mount
        }
    })

    //stateDidSomething
    useEffect(() => {
        console.log('color - DidMount'); //2 mount / update

        return () => {
            console.log('color - WillUnmount'); //1 un-mount
        }
    }, [color])

    console.log('render');
    return (
        <div>
            <h1 style={
                {
                    backgroundColor: color
                }
            }>LifeCycleFunc, {count}</h1>
            <button onClick={() => setCount(count + 1)}>Add</button>
            <button onClick={() => setColor(color === 'red' ? 'blue' : 'red')}>Change Color</button>
        </div>
    )
}

export default LifeCycleFunc;
