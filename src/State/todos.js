import axios from "axios";


//action
export const fetchTodos = () => {
    return async (dispatch) => {
        const resp = await axios.get('https://jsonplaceholder.typicode.com/todos');

        dispatch({
            type: 'FETCH_TODOS',
            payload: resp.data.slice(0, 15)
        })
    }
}

//will add a new item with a given title
export const postTodos = (title) => {
    return {
        type: 'POST_TODOS',
        payload: title
    }
}

//will toggle completed on an item by id
export const patchTodos = (id) => {
    return {
        type: 'PATCH_TODOS',
        payload: id
    }
}

//will delete an item by id
export const deleteTodos = (id) => {
    return {
        type: 'DELETE_TODOS',
        payload: id
    }
}


//reducer 
export const todosReducer = (list = [], action) => {
    const getIndex = () => list.findIndex(i => i.id === action.payload);

    switch (action.type) {
        case 'FETCH_TODOS': return action.payload;
        case 'POST_TODOS':
            return [...list, { id: Math.floor(Math.random() * 10000), title: action.payload, completed: false }];
        case 'PATCH_TODOS':
            var index = getIndex();
            list[index].completed = !list[index].completed;
            return [...list];
        case 'DELETE_TODOS':
            var index = getIndex();
            list.splice(index, 1);
            return [...list];
        default: return list;
    }
}