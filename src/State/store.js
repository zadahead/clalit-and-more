import { createStore, combineReducers, applyMiddleware } from "redux";

import ReduxThunk from "redux-thunk";

import { countReducer } from './counter';
import { todosReducer } from "./todos";

const reducers = combineReducers({
    count: countReducer,
    todos: todosReducer
})

const store = createStore(reducers, applyMiddleware(ReduxThunk));

export default store;
