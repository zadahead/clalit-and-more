
//actions
export const addCount = () => {
    return {
        type: 'ADD_COUNT',
        payload: 1
    }
}

//reducers
export const countReducer = (state = 0, action) => {
    switch (action.type) {
        case 'ADD_COUNT': return state + action.payload;
        default: return state;
    }
}