# Getting project to your system

### Clone Project
open `cmd` terminal and clone project to your folder 

```cmd
git clone https://gitlab.com/zadahead/clalit-and-more.git
```

get inside the created folder

```cmd
cd clalit-and-more
```

### Install

run npm-install

```cmd
npm install
```

### Edit and Load

to open project in VS-code write: 

```cmd
code .
```

to run project on your localhost

```cmd
npm start
```
